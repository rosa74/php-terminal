<?php

echo "Quelle table de multiplication souhaitez vous afficher ?\n";

$i = intval(fgets(STDIN));

echo "-------------\n";
echo "-- Table de $i\n";
echo "-------------\n";
for ( $j = 0 ; $j <= 10 ; $j++ ) {
    echo "$i * $j = ".($i*$j)."\n";
}
