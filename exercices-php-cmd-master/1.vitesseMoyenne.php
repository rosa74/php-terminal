<?php

/**
 * Ecrire un algorithme qui calcul la vitesse moyenne d'un déplacement
 * 1/ Demander à l'utilisateur de saisir la distance parcourue (en km)
 * 2/ Demander à l'utilisateur de saisir le temps pour effectuer le parcours (en mn)
 * 3/ Afficher la vitesse moyenne du déplacement en km/h
 */

$input = fgets(STDIN);

echo "Veuillez saisir la distance parcourue (en km) : ";
$i = intval(fgets(STDIN));
echo "Veuillez saisir le temps pour effectuer le parcours (en mn) : ";
$j = intval(fgets(STDIN));
echo "La vitesse moyenne sur le parcours est de : " . round($i / $j * 60) . " km/h \n";


/* solution 1

echo "Veuillez saisir la distance parcourue (en km) : \n";
$distanceParcourue=floatval(fgets(STDIN));

echo "Veuillez saisir le temps pour effectuer le parcours (en mn) : \n";
$tempsParcourt=intval(fgets(STDIN))/60;


if ($tempsParcourt === 0) {
    $vitesse=0;
}else{
$vitesse=round($distanceParcourue/$tempsParcourt,2);
}
echo "La vitesse moyenne sur le parcours est de : round$vitesse km/h \n"" 

*/

/* solution 2


echo "Veuillez saisir la distance parcourue (en km) : \n";
$distanceParcourue=intval(fgets(STDIN));

echo "Veuillez saisir le temps pour effectuer le parcours (en mn) : \n";
$tempsParcourt=intval(fgets(STDIN))/60;

if ($tempsParcourt <=0 || $distanceParcourue <= 0) {
    echo "Impossible ! \n;"
}else{
$vitesse=round($distanceParcourue/$tempsParcourt,2);
}

*/


?>