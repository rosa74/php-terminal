<?php

/*
ecrire le code permettant de dessiner les figures ci dessous
#####
#####
#####
#####
#####

#####
#   #
#   #
#   #
#####

#
##
# #
#  #
#####

#   #
 # #
  #
 # # 
#   #

  #  
 # #
#   #
 # #
  #

# # #
 # #
# # #
 # #
# # #

demander à l utilisateur la hauteur de la figure
demander à l utilisateur la lagueur de la figure
demander à l utilisateur le caractere utilise pour dessiner la figure
demander à l utilisateur quelle figure dessiner (valeur possible 1 à 6)
dessiner la figure
*/

echo " veuillez saisir la hauteur de la figure ? \n";
$hauteur = intval(fgets(STDIN));

echo " veuillez saisir la largueur de la figure ? \n";
$largueur = intval(fgets(STDIN));

echo " veuillez saisir le caractere utilisé pour dessiner la figure ? \n";
$caractere=trim(fgets(STDIN));

echo " quelle figure voulez vous ? \n ";
$figure= trim(fgets(STDIN));


switch ($figure){
    case 1:
        for ($j = 0; $j < $hauteur; $j++) {

            for ($i = 0; $i < $largueur; $i++) {
                echo $caractere;
            }
            echo "\n";
        }
        echo "\n\n\n";
    break;

    case 2:

        for ($j = 0; $j < $hauteur; $j++) {

            for ($i = 0; $i < $largueur; $i++) {

                if ($i == 0 || $i == $largueur - 1 || $j == 0 || $j == $hauteur - 1) {
                    echo $caractere;
                } else {

                    echo " ";
                }
            }
            echo "\n";
        }
        echo "\n\n\n";

        break;

    case 3:
        for ($j = 0; $j < $hauteur; $j++) {

            for ($i = 0; $i < $largueur; $i++) {

                if ($i == $j || $i == 0 || $j == $hauteur - 1) {
                    echo $caractere;
                } else {

                    echo " ";
                }
            }
            echo "\n";
        }
        echo "\n\n\n";

        break;

    case 4:
        for ($j = 0; $j < $hauteur; $j++) {

            for ($i = 0; $i < $largueur; $i++) {

                if ($i == $j || $i == ($hauteur - 1) - $j) {
                    echo $caractere;
                } else {

                    echo " ";
                }
            }
            echo "\n";
        }
        echo "\n\n\n";

        break;

    case 5:
        for ($j = 0; $j < $hauteur; $j++) {

            for ($i = 0; $i < $largueur; $i++) {

                if (
                    $i == $j + round(($largueur - 1) / 2)
                    || $i == round(($largueur - 1) / 2) - $j
                    || $j == $i + round(($hauteur - 1) / 2)
                    || $i == $hauteur - 1 - $j + round(($largeur - 1) / 2) )
                {
                    echo $caractere;
                } else {

                    echo " ";
                }
            }
            echo "\n";
        }
        echo "\n\n\n";
        break;

        case 6:
        for ($j = 0; $j < $hauteur; $j++) {

            for ($i = 0; $i < $largueur; $i++) {

                if (($i + $j) % 2 == 0) {
                    echo $caractere;
                } else {

                    echo " ";
                }
            }
            echo "\n";
        }
        echo "\n\n\n";
        break;
    default:
     echo "veuillez saisir un chiffre compris entre 1 et 6";

}
?