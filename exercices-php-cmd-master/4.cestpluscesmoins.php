<?php
/** 4.1
 * Ecrire le code d'un jeu permettant de deviner un nombre.
 * 1/ Demander à l'utilisateur de saisir le nombre minimum possible
 * 2/ Demander à l'utilisateur de saisir le nombre maximum possible
 * 3/ Le programme choisit un nombre aleatoire compris entre ces deux nombres
 * mais ne l'affiche pas à l'utilisateur
 * 4/ le programme demande à l'utilisateur de saisir un nombre et lui répond :
 * - C'est plus
 * - C'est moins
 * - Bravo la réponse était ...
 */

/** 4.2
 * 5/ Demander à l'utilisateur si il souhaite rejouer (sans quitter le programme)
 * si l'utilisateur rejoue, on conserve les valeurs min et max indiquées en 1/ et 2/
 */

/** 4.3
 * 6/ L'utilisateur dispose de 10 tentatives pour deviner le nombre
 * lorsqu'il dépasse 10 tentative le jeu se termine et indique :
 * - Vous avez perdu
 * puis demande à l'utilisateur si il souhaite rejouer
 */
/** 4.2
 * 5/ Demander à l'utilisateur si il souhaite rejouer (sans quitter le programme)
 * si l'utilisateur rejoue, on conserve les valeurs min et max indiquées en 1/ et 2/
 */
/** 4.3
 * 6/ L'utilisateur dispose de 10 tentatives pour deviner le nombre
 * lorsqu'il dépasse 10 tentative le jeu se termine et indique :
 * - Vous avez perdu
 * puis demande à l'utilisateur si il souhaite rejouer
 */
echo "Veuillez saisir le nombre minimum possible : ";
$mini = intval(fgets(STDIN));
echo "Veuillez saisir le nombre maximum possible : ";
$max = intval(fgets(STDIN));
$nbAleatoire = rand($mini, $max);
$rejouer = "Y";
do{
    if($rejouer == "Y"){
        echo "Devinez le nombre  : ";
        $nbAleatoire = rand($mini, $max);
        $tab = [];
        do {
            $i = intval(fgets(STDIN));
            $tab[] = $i;
            if($i < $nbAleatoire){
                echo "C'est plus que ".$i . " (".count($tab)."/10)\n";
            }elseif($i > $nbAleatoire){
                echo "C'est moins ".$i. " (".count($tab)."/10)\n";
            }else{
                echo "Bravo la réponse était $i et vous avez gagné en ".count($tab)." tentatives \n";
                echo "Rejouer ? (Y/n) \n";
                $rejouer = trim(fgets(STDIN));
                echo "------------------------\n";
            }
        } while ($i != $nbAleatoire);
    }
}while ($rejouer == "Y");

/*
echo "Quel est le nombre minimum possible ?\n";
$min = intval(fgets(STDIN));

do {
    echo "Quel est le nombre maximum possible ?\n";
    $max = intval(fgets(STDIN));
} while ( $max <= $min );

$jouer = true;

do {
    $nombreATrouver = mt_rand($min, $max);
    $nombreTentative = 0;

    do {
        $nombreTentative++;
        echo "Trouver le nombre entre $min et $max : (tentative $nombreTentative/10)\n";
        $n = intval(fgets(STDIN));
        if ( $n < $nombreATrouver ) {
            echo "C'est plus que $n\n";
        } elseif ( $n > $nombreATrouver ) {
            echo "C'est moins que $n\n";
        } else {
            echo "Bravo la réponse était $nombreATrouver (trouvé en $nombreTentative tentative".( $nombreTentative > 1 ? "s" : "" ).")\n";
        }
    } while( $n != $nombreATrouver && $nombreTentative < 10 );

    if ( $nombreTentative == 10 && $n != $nombreATrouver ) {
        echo "Vous avez perdu\n";
    }

    echo "Rejouer ? Y/n \n";
    $jouer = trim(fgets(STDIN)) === "n" ? false : true;

} while( $jouer );



*/

?>