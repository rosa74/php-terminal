<?php

/**
 * Ecrire un algorithme pour afficher le temps de cuisson d'une viande
 * en fonction du type de viande, du poids et de la cuisson souhaitée
 *
 * Règles à appliquer :
 * Pour cuire 500g de boeuf, il faut :
 *  - bleu : 10 minutes
 *  - à point : 17 minutes
 *  - bien cuit : 25 minutes
 * Pour cuire 400g de porc, il faut :
 *  - bleu : 15 minutes
 *  - à point : 25 minutes
 *  - bien cuit : 40 minutes
 * le temps de cuisson est proportionnel au poids
 * (ex: pour cuire 750g de boeuf bleu, il faut 15 minutes)
 *
 * 1/ Demander à l'utilisateur de saisir le type de viande (boeuf, porc)
 * 2/ Demander à l'utilisateur de saisir le poids de la viande à cuire en grammes
 * 3/ Demander à l'utilisateur de saisir la cuisson souhaitée (bleu, à point, bien cuit)
 * 4/ Afficher le temps de cuisson
 *
 * Attention: Lors de la saisie, tant que les valeurs ne sont pas valides,
 * poser de nouveau la question à l'utilisateur.
 * (ex: si le nom de la viande n'est pas "boeuf" ou "porc", demander à nouveau le type de viande)
 */
do {
    echo "Veuillez saisir le type de viande (boeuf / porc) : ";
    $viande = trim(fgets(STDIN));
} while ($viande != "boeuf" && $viande != "porc");
echo "Veuillez saisir le poids de la viande (en gr) : ";
$poids = intval(fgets(STDIN));
do {
    echo"Veuillez saisir la cuisson souhaitée (bleu, à point, bien cuit) : ";
    $cuisson = trim(fgets(STDIN));
} while ($cuisson != "bleu" && $cuisson != "à point" && $cuisson != "bien cuit");
$boeufBleu = $poids*0.02;
$boeufPt = $poids*0.034;
$boeufBc = $poids*0.05;
$porcBleu = $poids*0.0375;
$porcPt = $poids*0.0625;
$porcBc = $poids*0.1;
if($viande == 'boeuf'){
    if($cuisson == 'bleu'){
        echo "Le temps de cuisson est de : $boeufBleu mn \n";
    }elseif($cuisson == 'à point'){
        echo "Le temps de cuisson est de : $boeufPt mn \n";
    }else{
        echo "Le temps de cuisson est de : $boeufBc mn \n";
    }
}else{
    if($cuisson == 'bleu'){
        echo "Le temps de cuisson est de : $porcBleu mn \n";
    }elseif($cuisson == 'à point'){
        echo "Le temps de cuisson est de : $porcPt mn \n";
    }else{
        echo "Le temps de cuisson est de : $porcBc mn \n";
    }
}


/*
$typeViandePossible= ['boeuf','porc'];

do{
    echo "Veuillez saisir le type de viande ?(".implode(", ", $typesViandePossible)).")\n";
    $typeViande=trim(fgets(STDIN));
} while(!in_array($typeViande,$typesViandePossible)) ;


do{
    echo "Veuillez saisir le poids de la viande ?(en grammes) : \n";
    $poidsViande=intval(fgets(STDIN));
}while ($poidsViande <=0);


$typesCuissonPosssible= ['bleu','à point','bien cuit'];

do{
    echo "Veuillez saisir le type de cuisson ? (".implode(", ", $typesCuisson)).")\n";
    $typeCuisson=trim(fgets(STDIN));
} while(!in_array($typeCuisson,$typesCuissonPossible)) ;

// option 1 avec des conditions
if ( $typeDeViande === "boeuf" ) {
    if ( $typeDeCuisson === "bleu" ) {
        $tempsDeCuisson = 10 * $poidsDeLaViande / 500;
    } elseif ( $typeDeCuisson === "à point" ) {
        $tempsDeCuisson = 17 * $poidsDeLaViande / 500;
    } elseif ( $typeDeCuisson === "bien cuit") {
        $tempsDeCuisson = 25 * $poidsDeLaViande / 500;
    }
} elseif ( $typeDeViande === "porc" ) {
    if ( $typeDeCuisson === "bleu" ) {
        $tempsDeCuisson = 15 * $poidsDeLaViande / 400;
    } elseif ( $typeDeCuisson === "à point" ) {
        $tempsDeCuisson = 25 * $poidsDeLaViande / 400;
    } elseif ( $typeDeCuisson === "bien cuit") {
        $tempsDeCuisson = 40 * $poidsDeLaViande / 400;
    }
}


// option 2 avec des switch
switch($typeDeViande) {
    case 'boeuf':
        switch($typeDeCuisson) {
            case 'bleu':
                $tempsDeCuisson = 10 * $poidsDeLaViande / 500;
            break;
            case 'à point':
                $tempsDeCuisson = 17 * $poidsDeLaViande / 500;
            break;
            case 'bien cuit':
                $tempsDeCuisson = 25 * $poidsDeLaViande / 500;
            break;
        }
    break;
    case 'porc':
        switch($typeDeCuisson) {
            case 'bleu':
                $tempsDeCuisson = 15 * $poidsDeLaViande / 400;
            break;
            case 'à point':
                $tempsDeCuisson = 25 * $poidsDeLaViande / 400;
            break;
            case 'bien cuit':
                $tempsDeCuisson = 40 * $poidsDeLaViande / 400;
            break;
        }
    break;
}



// option 3 (meilleure) //

$poidReference=[
        'boeuf' =>500,
        'porc' =>400,
                ];

$tempsReference =[
        'boeuf' =>[
            'bleu'=>10,
            'à point'=>17,
            'bien cuit'=>25,
        ],

        'porc'=>[
                'bleu'=>15,
                'à point'=>25,
                'bien cuit'=>40,
        ],
    ];


$tempsCuisson = $tempsReference[$typeViande][$typeCuisson] * $poidsViande /$poidReference[$typeViande];
echo "Le temps de cuisson est de : $tempsCuisson mn\n";

?>