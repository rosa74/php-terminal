<?php

/*
creer le code permettant de simuler le fonctionnement d'une machine à café

le café coûte 60cts
la machine accepte les pieces de 2€, 1€, 0.5€, 0.2€ , 0.1€ et 0.05€

1/ demander à l'utilisateur de saisir la valeur de sa pièce
 - ne pas accepter les pieces non autorisées
 - tant que le montant n'est pas supérieur ou égal au prix du café
 demander à l'utilisateur de saisir une  autre piece (le montant doit se cumuler)
2/ servir le cagé (bien chaud!)
3/ rendre la monnaie 
    - on suppose que le stock de pièce de la machine n'est pas limité
    - rendre le moins de pièces possibles
*/

$prixCafe = 60;
$piecesAcceptes = [
    "2e" => 200,
    "1e" => 100,
    "50cts" => 50,
    "20cts" => 20,
    "10cts" => 10,
    "5cts" => 5,
];


do{


    $piecesInseres = [];

echo " Le café coûte 60cts \n";
$prixCafe=60;

echo "La machine accepte les pieces suivantes : 2€, 1€, 0.5€, 0.2€ , 0.1€ et 0.05€ \n";

do{
  echo  " veuillez saisir la valeur de votre piece : ";
  $valeurPieces=trim(fgets(STDIN));
  if(!array_key_exists($valeurPieces, $piecesAcceptes)){
    echo "piece non acceptée \n";
  } else{
    $piecesInseres[]=$piecesAcceptes[$valeurPieces];

    if (array_sum($piecesInseres) >= $prixCafe) {
        echo " voici votre café bien chaud ! 
        (   ) )
         ) ( (
       _______)_
    .-'---------|  
   ( C|/\/\/\/\/|
    '-./\/\/\/\/|
      '_________'
       '-------'
---------------------\n";

            if (array_sum($piecesInseres) > $prixCafe) {

                $monnaie = array_sum($piecesInseres) - $prixCafe;
                echo " voici votre monnaie : ".$monnaie. "\n";

                
            foreach ($piecesAcceptes as $cle => $valeur) {
               while ($valeur <= $monnaie){
                    echo "Voici votre pièce de ".$cle."\n";
                    $monnaie = $monnaie - $valeur;
                }
            }
        }
    }else {
    echo "credit insuffisant ". array_sum($piecesInseres)." \n";
    }
}
}
while(!array_key_exists($valeurPieces,$piecesAcceptes) || array_sum($piecesInseres) <= $prixCafe);

}while(true);

?>